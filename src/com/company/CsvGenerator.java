package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGenerator implements CsvGeneratorInterface {

    @Override
    public void toCsv(List<String> lines)
    {
        try {
            FileWriter CsvW = new FileWriter("new.csv");

            // pretty line
            CsvW.append("*************");
            CsvW.append("\n");
            // new line + '\n'
            for (String line: lines) {
                CsvW.append(line);
            }

            CsvW.append("\n");
            // pretty line
            CsvW.append("*************");
            CsvW.append("\n");
            CsvW.flush();
            CsvW.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
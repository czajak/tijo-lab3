package com.company;

import java.io.IOException;
import java.util.List;

public interface CsvGeneratorInterface {
    void toCsv(List<String> l);
}

package com.company.zadanie2;

public class MaterialPoint2D extends Point2D{
    private double mass;

    public MaterialPoint2D() {
    }

    public MaterialPoint2D(double pos_x, double pos_y, double mass) {
        super(pos_x, pos_y);
        this.mass = mass;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "MaterialPoint2D{" +
                "point2D=" + super.toString() +
                ", mass=" + mass +
                '}';
    }
}

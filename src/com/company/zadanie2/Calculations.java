package com.company.zadanie2;


public class Calculations {
    public static Point2D positionGeometricCenter(Point2D[ ] points){
        Point2D geoCenter = new Point2D();
        double pos_x = 0;
        double pos_y = 0;
        for (Point2D point: points) {
            pos_x+= point.getPos_x();
            pos_y+= point.getPos_y();
        }
        return new Point2D(pos_x / points.length,pos_y / points.length);
    }

    public static MaterialPoint2D positionCenterOfMass(MaterialPoint2D[ ] materialPoints){
        MaterialPoint2D geoCenter = new MaterialPoint2D();
        double pos_x = 0;
        double pos_y = 0;
        double mass = 0;
        for (MaterialPoint2D point: materialPoints) {
            pos_x+= point.getPos_x();
            pos_y+= point.getPos_y();
            mass+= point.getMass();
        }
        return new MaterialPoint2D(pos_x / mass, pos_y / mass, mass);
    }
}

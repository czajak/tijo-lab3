package com.company.zadanie2;

public class Point2D {
    protected double pos_x;
    protected double pos_y;

    public Point2D() {
    }

    public Point2D(double pos_x, double pos_y) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
    }

    public double getPos_x() {
        return pos_x;
    }

    public void setPos_x(double pos_x) {
        this.pos_x = pos_x;
    }

    public double getPos_y() {
        return pos_y;
    }

    public void setPos_y(double pos_y) {
        this.pos_y = pos_y;
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "pos_x=" + pos_x +
                ", pos_y=" + pos_y +
                '}';
    }
}

package com.company;

import java.util.List;

public interface XmlGeneratorInterface {
    void toXml(List<String> l);
}
